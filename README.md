# VTK-m benchmark records

The logs are recorded in an independent branch of this repo named `records`.

The format of the record is:

- Filename to be the full hash of the commit (plus .json).
- The content of the file is the JSON Google Bench output of the benchmark.

This repository is actively maintained by the CI builds of VTK-m.
